package app

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
	httpSwagger "github.com/swaggo/http-swagger"

	_ "gitlab.com/nergilz/todo-service/app/docs"

	"gitlab.com/nergilz/todo-service/app/cmd/app/config"
	"gitlab.com/nergilz/todo-service/app/pkg/logging"
	metricheartbeat "gitlab.com/nergilz/todo-service/app/pkg/metricHeartbeat"
)

type App struct {
	httpServer *http.Server
	logger     *logging.Logger
	cfg        *config.Config
	router     *httprouter.Router
}

func NewApp(logger *logging.Logger, config *config.Config) (App, error) {
	logger.Logg.Info("route init")

	router := httprouter.New()
	logger.Logg.Info("swagger init")
	router.Handler(http.MethodGet, "/swagger", http.RedirectHandler("/swagger/index.html", http.StatusMovedPermanently))
	router.Handler(http.MethodGet, "/swagger/*any", httpSwagger.WrapHandler)

	metricHandler := metricheartbeat.Handler{}
	metricHandler.Register(router)

	app := App{
		cfg:    config,
		logger: logger,
		router: router,
	}

	return app, nil
}

func (a *App) Run() {
	a.startHttp()
}

func (a *App) startHttp() {
	a.logger.Logg.Info("starts http server")

	var listener net.Listener
	var err error

	if a.cfg.Listen.Type == "port" {
		a.logger.Logg.Infof("bind app to host: %s on port: %s", a.cfg.Listen.BindIP, a.cfg.Listen.Port)
		listener, err = net.Listen("tcp", fmt.Sprintf("%s:%s", a.cfg.Listen.BindIP, a.cfg.Listen.Port))
		if err != nil {
			a.logger.Logg.Fatal(err)
		}
	}
	// todo - else "sock"

	c := cors.New(cors.Options{
		AllowedMethods: []string{http.MethodGet, http.MethodPost},
	})
	handler := c.Handler(a.router)

	a.httpServer = &http.Server{
		Handler:      handler,
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
	}

	// todo - graceful shutdown

	a.logger.Logg.Info("application initialized and started")

	err = a.httpServer.Serve(listener)
	if err != nil {
		switch {
		case errors.Is(err, http.ErrServerClosed):
			a.logger.Logg.Warn("server shutdown")
		default:
			a.logger.Logg.Fatal(err)
		}
	}

	err = a.httpServer.Shutdown(context.Background())
	if err != nil {
		a.logger.Logg.Fatal(err)
	}
}
