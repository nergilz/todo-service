package config

import (
	"sync"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type Config struct {
	IsDebug   bool `env:"IS_BEBUG" env-default:"false"` // tags for "https://github.com/ilyakaznacheev/cleanenv"
	Listen    Listen
	AppConfig AppConfig
}

type Listen struct {
	Type   string `env:"TYPE" env-default:"port" env-description:"port of sock"`
	BindIP string `env:"BIND_IP" env-default:"0.0.0.0"`
	Port   string `env:"PORT" env-default:"10000"`
}

type AppConfig struct {
	LogLevel  string `env:"LOG_LEVEL" env-default:"info"`
	AdminUser AdminUser
}

type AdminUser struct {
	Email    string `env:"ADMIN_EMAIL" env-default:"admin"`
	Password string `env:"ADMIN_PWD" env-default:"admin"`
}

var instanceConf *Config
var onceConf sync.Once // make singlton

func GetConfig(log *logrus.Logger) *Config {
	onceConf.Do(func() { // второй раз не прочесть enveronment
		err := cleanenv.ReadEnv(instanceConf)
		if err != nil {
			helpEnvText := "notes system"
			help, _ := cleanenv.GetDescription(instanceConf, &helpEnvText)
			log.Print(help)
			log.Fatal(errors.Errorf("can not read config: ", err)) // todo - разобраться почему падает cleanenv.ReadEnv
		}
	})
	return instanceConf
}

// todo - убрать хардкод
func InitConfig(log *logrus.Logger) *Config {
	return &Config{
		IsDebug: false,
		Listen: Listen{
			Type:   "port",
			BindIP: "0.0.0.0",
			Port:   "10000",
		},
		AppConfig: AppConfig{
			LogLevel: "trace",
			AdminUser: AdminUser{
				Email:    "admin@dot.com",
				Password: "qwerty",
			},
		},
	}
}
