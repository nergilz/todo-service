package main

import (
	"gitlab.com/nergilz/todo-service/app/cmd/app/config"
	"gitlab.com/nergilz/todo-service/app/internal/app"
	"gitlab.com/nergilz/todo-service/app/pkg/logging"
)

func main() {
	log := logging.InitLogger()
	log.Logg.Info("run app")

	// cfg := config.GetConfig(log.Logg)
	cfg := config.InitConfig(log.Logg)
	log.Logg.Info("config initialization")

	app, err := app.NewApp(log, cfg)
	if err != nil {
		log.Logg.Fatal(err)
	}

	app.Run()

}
