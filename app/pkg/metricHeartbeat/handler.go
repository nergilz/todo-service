package metricheartbeat

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

const (
	urlTest = "/api/test"
)

type Handler struct {
}

func (h *Handler) Register(router *httprouter.Router) {
	router.HandlerFunc(http.MethodGet, urlTest, h.Heartbeat)
}

// Heartbeat godocs
// @Summary Heartbeat metric
// @Tags Metric
// @Success 200
// @Failure 400
// @Router /api/test [get]
func (h *Handler) Heartbeat(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("metric hearbeat"))
}
